import mock as mock
import pytest
from mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService


def person():
    return Person("imie", "nazwisko", "pesel")


def pesels():
    return [92010199999, 82010199999, 72010199999]


# Pobranie pierwszego numeru PESEL dla imienia i nazwiska,
@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=pesels()))
def test_get_pesel():
    c = Catalog(DB, None)

    result = c.get_pesel("imie", "nazwisko")

    assert result == pesels()[0]
    DB.get_pesel_list.assert_called_with("imie", "nazwisko")


# Pobranie listy numerów PESEL dla imienia i nazwiska,
@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=pesels()))
def test_get_pesel_list():
    c = Catalog(DB, None)

    result = c.get_pesel_list("imie", "nazwisko")

    assert result == pesels()
    DB.get_pesel_list.assert_called_with("imie", "nazwisko")


# Wyszukanie osoby po numerze pesel,
@mock.patch('catalog.db.DB.get_person', MagicMock(return_value=person()))
def test_should_get_person_by_pesel():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_person("pesel")

    # then
    assert_equal_person(result, person())
    DB.get_person.assert_called_with("pesel")


def assert_equal_person(result, expected):
    assert result.get_name() == expected.get_name()
    assert result.get_surname() == expected.get_surname()
    assert result.get_pesel() == expected.get_pesel()


# Dodanie osoby do katalogu, poprawny PESEL.
@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=True))
@mock.patch('catalog.db.DB.add_person', MagicMock())
def test_add_person_correct_pesel_should_be_added():
    c = Catalog(DB, PeselService)

    c.add_person("imie", "nazwisko", "81010122222")

    PeselService.verify.assert_called_with("81010122222")

    args = DB.add_person.call_args.args
    assert len(args) == 1
    assert args[0].__class__ == Person

    expected = Person("imie", "nazwisko", "81010122222")
    assert_equal_person(args[0], expected)


# Dodanie osoby do katalogu, niepoprawny PESEL.
@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=False))
@mock.patch('catalog.db.DB.add_person', MagicMock())
def test_add_person_incorrect_pesel_should_raise():
    c = Catalog(DB, PeselService)

    with pytest.raises(Exception):
        c.add_person("imie", "nazwisko", "kotek")

    PeselService.verify.assert_called_with("kotek")
    DB.add_person.assert_not_called()
