CMD=`command -v python3`
if [ "$CMD" != '' ]
then
	PYTHON=python3
fi
CMD=`command -v py`
if [ "$CMD" != '' ]
then
	PYTHON=py
fi

$PYTHON -m unittest tests/unittest_catalogtest.py
$PYTHON -m pytest tests/pytest_catalogtest.py

