package pl.poznan.put.spio.service;

public interface PESELService {

    boolean verify(String pesel);

}
